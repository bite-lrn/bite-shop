import Vue from 'vue'
import App from './App'
// 引入全局状态管理
import store from './store'
// 引入uview框架
import uView from "uview-ui";
Vue.use(uView);
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
  ...App
})
app.$mount()

