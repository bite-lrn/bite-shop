import Vue from 'vue'
import Vuex from 'vuex'
import state from './state.js'   //导入vuex数据源
import getters from './getter.js'
import mutations from './mutation.js'
import actions from './action.js'
Vue.use(Vuex);//vue的插件机制
//Vuex.Store 构造器选项
const store = new Vuex.Store({
  state,
	getters,
	mutations,
	actions
})
export default store