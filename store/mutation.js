import state from './state.js' //导入vuex数据源
const mutations = {
	// 设置token
	setToken:(state, token)=>{
		state.token = token;
	},
	// 切换用户
	changeUser: (state, currentUser) => {
		state.currentUser = currentUser;
	},
}
// 导出state
export default mutations
