import state from './state.js'   //导入vuex数据源
const getters = {
	token: state => state.token,
	currentUser: state => state.currentUser,
	statusBarHeight: state => state.statusBarHeight,
	navbarHeight: state => {
		if(state.platform === 'H5'){
			state.navbarHeight = 0
		}else{
			state.navbarHeight = 40
		}
		return state.navbarHeight
	},
	platform: state => state.platform,
	rules:state => state.rules,
}
// 导出state
export default getters