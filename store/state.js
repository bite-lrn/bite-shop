let baseUrl='https://aqq.certca.cn';
const state = {
	baseurl:baseUrl,
	token:  '',
	currentUser: {}, //当前身份信息
	background: {
		// 导航栏背景图
		// background: 'url(https://cdn.uviewui.com/uview/swiper/1.jpg) no-repeat',
		// 还可以设置背景图size属性
		// backgroundSize: 'cover',
		// 渐变色
		// backgroundImage: 'linear-gradient(to right, #358af0, #1e65fe)'
		background: '#6d78f7'
	},
	activeColor: '#6d78f7',//被激活时的主题色1
	errorColor: '#fa3534',//被激活时的主题色2
	successColor: '#11b760',//被激活时的主题色3
	titleColor: '#ffffff', //顶部导航栏文字颜色
	statusBarHeight: uni.getSystemInfoSync().statusBarHeight, //状态栏的高度，H5中，此值为0，因为H5不可操作状态栏
	navbarHeight: 0, // 导航栏内容区域高度，不包括状态栏高度在内
	platform: uni.getSystemInfoSync().VUE_APP_PLATFORM, //当前平台类型
	rules: { //表单校验规则
		cardholderName: [{
				required: true,
				message: '姓名不可以为空',
				trigger: ['change', 'blur'],
			},
			{
				min: 2,
				max: 5,
				message: '姓名长度在2到5个字符',
				trigger: ['change', 'blur'],
			},
			{
				validator: (rule, value, callback) => {
					return /[\u4e00-\u9fa5]/.test(value);
				},
				message: '姓名必须为中文',
				trigger: ['change', 'blur'],
			},
		],
		date: [{
			required: true,
			message: '请选择时间',
			trigger: ['change', 'blur', 'input'],
		}, ],

		mobile: [{
				required: true,
				message: '请填写手机号',
				trigger: ['change', 'blur'],
			},
			{
				validator: (rule, value, callback) => {
					if(/^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])[*]{4}[0-9]{4}$/.test(value)){ 
						return value ===uni.getStorageSync('mobile')
					}else{
						return /^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/.test(value);
					}
				},
				message: '手机号码不正确',
				trigger: ['change', 'blur'],
			}
		],
		code: [{
				required: true,
				message: '请填写验证码',
				trigger: ['change', 'blur'],
			},
			{
				type: 'number',
				message: '请填写正确的验证码',
				trigger: ['change', 'blur'],
			}
		],
		password: [{
				required: true,
				message: '请填写密码',
				trigger: ['change', 'blur'],
			},
			{
				min: 6,
				max: 6,
				message: '请将密码设置为6位数字',
				trigger: ['change', 'blur'],
			},
			{
				type: 'number',
				message: '请将密码设置为6位数字',
				trigger: ['change', 'blur'],
			},

			{
				validator: (rule, value, callback) => {
					password = value
					return true
				},
				trigger: ['change', 'blur'],
			}
		],
		rePassword: [{
				required: true,
				message: '请重新填写密码',
				trigger: ['change', 'blur'],
			},
			{
				validator: (rule, value, callback) => {
					return value === password;
				},
				message: '两次填写的密码不相等',
				trigger: ['change', 'blur'],
			}
		],
	},
	errorType: ['message', 'border-bottom'], //表单错误提示方式
}
// 导出state
export default state
